

# ----------------------------------------------------
# Command-line utility for exportin a sqlite3 database to xlsx
# ----------------------------------------------------
# Usage:
#        python sqlite3_export.py [DB_FILENAME]
# ----------------------------------------------------
# Example:
#        python sqlite3_export.py test.db
# ----------------------------------------------------

import sys, sqlite3, time
from pyexcelerate import Workbook, Style, Fill, Color, Font
from datetime import datetime

# -- Provides -- # 
def exportDatabase(inFile, table, sizes=None):
    """
    Export the database specified by inFile to the csv as specified by outFile

    params
           inFile <String> name of the database 
           outFile <String> name of the csv to export to

    returns void
    """
    with sqlite3.connect(inFile) as connection:
        outFile = inFile[:-3] + ".xlsx"
        cursor = connection.cursor()
        tuples = cursor.execute("SELECT * from "  + table + ";").fetchall()
        header = [d[0] for d in cursor.description]
        length = len(tuples)+1
        workbook = Workbook()
        sheet = workbook.new_sheet(table)

        # Note that this module starts at [1][1] not [0][0]
        # Export Header
        sheet.set_row_style(1, Style(size=25,
                                     font=Font(bold=True, color=Color(245,245,245)),
                                     fill=Fill(background=Color(100,100,100))))
        for col in range(1,len(header)+1):
            sheet.set_cell_value(1, col, header[col-1].upper())
            if (sizes): sheet.set_col_style(col, Style(size=sizes[col-1]))
            
        # Export Tuples
        for row in range (2, length+1):
            for col in range(1, len(header)+1):
                if (not (row % 100) or ((row+1)==length)):
                    sys.stdout.write("\rExporting row %d of %d " % (row, length-1) )
                    sys.stdout.flush()
                cell = tuples[row-2][col-1]
                 
                sheet.set_cell_value(row, col, cell)

        print "\nSaving workbook to " + outFile
        workbook.save(outFile)


if __name__ == "__main__":
    if not (len(sys.argv) > 0):
        print """
        # ----------------------------------------------------
        # Command-line utility for exportin a sqlite3 database to csv
        # ----------------------------------------------------
        # Usage:
        #        python export.py [DB_FILENAME] [DB_TABLE]
        # ----------------------------------------------------
        # Example:
        #        python export.py test.db grantees
        # ----------------------------------------------------
        """
        exit()

    sizes = [12, 20, 20, 13, 7, 7,
             6, 6, 6, 6, 6, 6, 6, 6, 6,
             14, 14, 20, 40, 40, 100, 100]
    
    exportDatabase(sys.argv[1], sys.argv[2], sizes)
