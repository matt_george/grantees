


# --------------------------------------------------------------------------
# Command-line utility for creating a database consisting for grantees
# --------------------------------------------------------------------------
# Usage:
#        python create.py [DB_NAME]
# --------------------------------------------------------------------------
# Example:
#        python create.py test.db
# --------------------------------------------------------------------------


import sqlite3, sys


# -- Private -- #
def _create_table(db):
    """
    Create a database table called "grantees" for the selected db with the columns:
       gid                     Text                 the id fo the grantee
       name                    Text                  of the the grantee
       url                     Text                  of the parsed page
       city                    Text                  of the grantee
       state                   Text                  of the grantee
       t_year                  Text                  year of the triennial report
       head_start              bool
       early_head_start        bool
       early_head_start_child        bool
       AIAN_head_start         bool
       AIAN_early_head_start   bool 
       AIAN_early_head_child   bool 
       MSHS_head_start         bool
       MSHS_early_head_start   bool
       MSHS_early_head_start_child   bool
       tel1                     text
       tel2                    text
       website                 text
       address1                Text(List[Sep=";"])   first parsed address
       address2                Text(List[Sep=";"])   second parsed address (if it exists)
       paths                   Text(List[Sep=";"])   relative path associated with the downloaded report
       report_names            Text(List[Sep=";"])   list of all report names


    return void
    """
    c = db.cursor()
    c.execute(''' create table grantees 
                  (gid text primary key, 
                   name text, 
                   url text, 
                   city text, 
                   state text,
                   t_year text,
                   hs integer,
                   ehs integer,
                   ehsc integer,
                   ahs integer,
                   aehs integer,
                   aehsc integer,
                   mhs integer,
                   mehs integer,
                   mehsc integer,
                   tel1 text,
                   tel2 text,
                   website text,
                   address1 text,
                   address2 text,
                   paths text,
                   report_names text
                   )''')
    db.commit()
    db.close()


if __name__ == "__main__":
    if not (len(sys.argv) > 1):
        print "Please specify the name of the database to be created"
        exit()

    _create_table(sqlite3.connect(sys.argv[1]))
    print "Created new database: ", sys.argv[1]
