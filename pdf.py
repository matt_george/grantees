# 
# Utility for extracting date information from processed pdfs in /pdfs
# Must have run ./convert.sh in order to convert from pdf -> txt
# ------------------------------------------------------------------


import re
import os
import sqlite3
import sys


RE_DATERANGE = re.compile('From (\d+/\d+/\d+) to (\d+/\d+/\d+)')
RE_ENROLL = re.compile('Funded Enrollment (\w+): (\d+)', re.IGNORECASE)
RE_TRIREPORT = re.compile('Triennial_Report', re.IGNORECASE)

def extractDateRange(pages):
    '''
    Searches pages for a date range in the from RE_DATERANGE, and if it exists,
    returns the first date in the range.
    
    in <string> pages
    out <string> {first date of range} | None
    '''
    return re.findall(RE_DATERANGE, pages)[0]


def extractEnrollment(pages):
    return re.findall(RE_ENROLL, pages);


def filterTextOnly(files):
    out = []
    for f in files:
        fname, ext = os.path.splitext(f)
        if re.search(RE_TRIREPORT, fname) == None: continue
        if ext == ".txt": out.append(f)
    return out
        

def processFile(path):
    print path
    with open(path) as inFile:
        content = inFile.read()
        dateRange = extractDateRange(content)
        enroll = extractEnrollment(content)
        return dateRange, enroll


def enrollToQuery(tups):
    """
    Generates an update clause of the form 'EHS = NUM, HS = NUM' from a list of tuples
    of the form [('HS', 'NUM'), ...]
    """
    print ", ".join(["=".join(tup) for tup in tups])

    
def insert(cursor, gid, tup):
    cursor.execute(
        'UPDATE grantees' +
        'SET ' + enrollquery +
        'WHERE gid == ?',
        gid)

        

def main():
    # Summary Errors
    S_UNABLE_TO_FIND_DATE = [] # if extractDateRange returns None

    size = len([x for x in os.walk("./pdfs/")])
    print "Found", size, "files to parse."

    #    with sqlite3.connect(dbFile) as connection:
    #       cursor = connection.cursor()
    for root, dirs, files in os.walk("./pdfs/"):
        gid = root.split("/")[-1]
        print "Working on:", gid
        for f in filterTextOnly(files):
            
            res = processFile('%s/%s' % (root, f))
            print res
            if (res):
                dateRange, enroll = res
                print enrollToQuery(enroll)
            else: S_UNABLE_TO_FIND_DATE.append(gid)
                


if __name__ == "__main__":
    dbFile = sys.argv[1]
    main(dbFile)
