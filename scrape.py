

from lxml import html
import requests
import re
import os
import sqlite3
import sys

ROOT_URL = 'https://eclkc.ohs.acf.hhs.gov'

summary = [] # for storing error information


def main(inFile, index=None):
    with sqlite3.connect(inFile) as connection:
        cursor = connection.cursor()
        page = requests.get(ROOT_URL +
                            '/hslc/data/psr/results?findGrantName=&findGrantState='+
                            '&findGrantNumber=&search=true&findGrantNameBasic=&findGrantNumberAdv=')
        tree = html.fromstring(page.content)
        grantees = tree.xpath('//div[@class="grantee col-lg-12 col-md-12 col-sm-12 col-xs-12"]')
        
        count = 0
        for g in grantees:
            if index and count == index: break
            insertTuple(cursor, parseGrantee(g))
            count +=1 
            connection.commit()

    # print any error information
    for s in summary:
        print s

        
## -- DB Insertion -- ##
def insertTuple(cursor, tup):
    cursor.execute('insert or ignore into grantees values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', tup)


## -- Parsing -- ##
def parseGrantee(grantee):
    '''
    Parses the selected grantee.
    
    Returns a tuple containing:
       gid                     the id fo the grantee
       name                    of the the grantee
       url                     of the parsed page
       city                    of the grantee
       state                   of the grantee
       t_year                  year of the triennial report
       head_start              bool
       early_head_start        bool
       early_head_start_child        bool
       AIAN_head_start         bool
       AIAN_early_head_start   bool 
       AIAN_early_head_start_child   bool 
       MSHS_head_start         bool
       MSHS_early_head_start   bool
       MSHS_early_head_start_child   bool
       telephone
       website
       address1                first parsed address
       address2                second parsed address (if it exists)
       paths                   relative path associated with the report
       report_names            list of all report names

    '''
    grantee__name = grantee.find_class("col-xs-12 col-md-12 col-lg-12 grantee__name")[0]
    iterlink = grantee__name.iterlinks().next()
    name = iterlink[0].text_content()
    print ""
    print "Parsing", name
    print "--------------------------------"
    url = ROOT_URL + iterlink[2]

    types = map(lambda t: t.xpath('p/text()'),
                grantee.find_class("col-lg-8 col-md-8 col-xs-12 grantee__type"))[0]

    grantee__info = grantee.find_class("col-lg-4 col-md-4 col-xs-12 grantee__info")[0]
    gid = grantee__info.xpath('p[@class="grantee__number"]/text()')[0]
    location = grantee__info.xpath('p[@class="grantee__city"]/text()')[0].split(',')
    city = location[0]
    state = location[1].strip()

    tel1, tel2, website, address1, address2 = parseAddresses(name, gid, html.fromstring(requests.get(url).content))
    report_year, paths, all_report_names = parseDetailPage(html.fromstring(requests.get(url).content))
    hs, ehs, ehsc, ahs, aehs, aehsc, mhs, mehs, mehsc = tagPrograms(types)
    
    downloadPDFs(gid, paths)
    
    return (gid, name, url, city, state, report_year, hs, ehs, ehsc, ahs, aehs, aehsc, mhs, mehs, mehsc,
            tel1, tel2, website, address1, address2, ";".join(toRelPath(gid, paths)), all_report_names)


def tagPrograms(types):
    hs=ehs=ehsc=ahs=aehs=aehsc=mhs=mehs=mehsc=0
    for ty in types:
        if ty == 'Head Start': hs=1
        elif ty == 'Early Head Start - Child Care Partnership': ehsc=1
        elif ty == 'Early Head Start': ehs=1
        elif ty == 'American Indian and Alaska Native Head Start': ahs=1
        elif ty == 'American Indian and Alaska Native Early Head Start - Child Care Partnership': aehsc=1
        elif ty == 'American Indian and Alaska Native Early Head Start': aehs=1
        elif ty == 'Migrant and Seasonal Head Start': mhs=1
        elif ty == 'Migrant and Seasonal Early Head Start - Child Care Partnership': mehsc=1
        elif ty == 'Migrant and Seasonal Early Head Start': mehs=1
    return hs, ehs, ehsc, ahs, aehs, aehsc, mhs, mehs, mehsc
            

def parseDetailPage(tree):
    report_links = tree.xpath('//div[contains(@class, "col-lg-12 col-md-12 col-sm-12 col-xs-12")]' +
                              '//a[contains(text(),"CLASS") or contains(text(), "Class") ' +
                              'or contains(text(), "Triennial")]')
    all_report_names = tree.xpath('//div[contains(@class, "col-lg-12 col-md-12 col-sm-12 col-xs-12")]//a[contains(@href,"javascript")]')
    all_report_names = map(lambda x: x.text_content().strip(), all_report_names)
    
    report_names = map(lambda x: x.text_content().strip(), report_links)
    report_year = getTriennialReportYear(report_names)
    paths = getDownloadPaths(tree, report_links)
    return report_year, zip(spaceToUnderscore(report_names), paths), ";".join(all_report_names)

def parseAddresses(name, gid, tree):
    programInfo = tree.xpath('//div[@class="programInfo"]')

    addresses = []
    for info in programInfo:
        address = info.xpath('//div[@class="programInfo"]//a[contains(@title, "Show")]')
        for a in address:
            if a not in addresses:
                addresses.append(a)

    # If more than 2 addresses, append to summary
    if len(addresses) > 2:
        summary.append(name + "(" + gid + ") had " + str(len(addresses))+ "addresses")
        
    addresses = map(lambda x: parseAddress(x), addresses)


    tel1=tel2=None
    
    tels = info.xpath('//div[@class="programInfo"]//a[contains(@href, "tel")]/text()')
    if len(tels)==1: tel1 =tels[0]
    elif len(tels)==2 and tels[0] != tels[1]:
        tel1 = tels[0]
        tel2 = tels[1]
    
    website = info.xpath('//div[@class="programInfo"]//a[contains(@href, "http")]/@href')
    if website != []: website = website[0]
    else: website = None
    
    if (len(addresses) > 1):
        return (tel1, tel2, website, addresses[0], addresses[1])


    return (tel1, tel2, website, addresses[0], None)


def toRelPath(gid, paths):
    'Returns the relative path of the downloaded filed'
    return map(lambda path: '/' + str(gid) + '/' + path[0], paths)


def parseAddress(address):
    content =  address.xpath('text()')
    return ";".join(filter(lambda s: s != "",
                           map(lambda text: text.strip(), content)))
    


def getTriennialReportYear(report_names):
    for name in report_names:
        if (re.search(r'Triennial', name)):
            return re.findall(r'\d+', name)[0]
    return None


def getDownloadPaths(tree, report_links):
    paths = []
    tokenized_links = map(lambda x: x.xpath("@onclick")[0].split("."), report_links)
    for link in tokenized_links:
        for i in range(0,len(link)):
            if link[i] == 'document':
                paths.append(link[i+1])
    return map(lambda path: tree.xpath('//form[@name="' +
                                       path +
                                       '"]/input[@name="reportType"]/@value')[0], paths)


def downloadPDFs(gid, pairs):
    'Downloads the pdfs for each pair, under a directory with the passed gid'
    for pair in pairs:
        downloadPDF(gid, pair[1],pair[0])

        
def downloadPDF(gid, url, name):
    print "downloading", name, url
    res = requests.get(url)

    path = "./pdfs/" + str(gid)
    if not os.path.isdir(path): os.mkdir(path)
    
    with open(path + "/" + name + ".pdf", 'w+') as outFile:
        outFile.write(res.content)


# Utilities
def spaceToUnderscore(names):
    return map(lambda x: x.replace(" ", "_"), names)


if __name__ == "__main__":
    dbFile = sys.argv[1]
    index = None
    if len(sys.argv) > 2: index = int(sys.argv[2])
    
    main(dbFile, index)
    
