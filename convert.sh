#!/bin/bash

clear

echo "Converting all PDFs in directory"

count=$(find ./pdfs/ -name 'Triennial_Report*.pdf' | wc -l)
i=1

for f in $(find ./pdfs/ -name 'Triennial_Report*.pdf')
do
    echo "Processing file $i of $count : $f" 

    dname=$(dirname $f)
    bname=$(basename $f .pdf)
    out_temp="temp"
    out="$dname/$bname.txt"
    
    convert -density 300 $f -type Grayscale -depth 32 -background white +matte $out_temp

    limit=0
    for img in $(find . -name "temp*")
    do
        imgtoadd="$out_temp-$limit"
        echo "Adding image $imgtoadd to $out"
        if [ $limit == 6 ]
        then
            break
        fi
        ((limit++))
        tesseract $imgtoadd ->> $out 
    done
    rm temp*
    ((i++))
done

echo "Done! Converted $count files"
